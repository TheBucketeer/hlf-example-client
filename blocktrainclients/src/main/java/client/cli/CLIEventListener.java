package client.cli;

import client.PiClientEventListener;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;

@RequiredArgsConstructor
@Getter
public class CLIEventListener extends PiClientEventListener {

    private final CLIClient client;

    public void handleEvent(JSONObject event) {
        this.p("CLIEventListener received event: " + event.toString());
    }
}

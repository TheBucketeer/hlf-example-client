package client.cli;


import client.PiClient;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import shared.GlobalConfig.CcFunction;


public class CLIClient extends PiClient {

    private String promptStr;
    private boolean wasInPrompt;

    private CLIClient(String clientSettingsFile) throws Exception {
        super(clientSettingsFile);
        this.setDefaultAnyCompositeEventListener();
        this.initChannel();
        CLIEventListener chaincodeEventHandler = new CLIEventListener(this);
        Thread thread = new Thread(chaincodeEventHandler);
        thread.start();
    }

    public void p(Object o) {
        this.p(o, false);
    }

    protected synchronized void p(Object o, boolean isPrompt) {
        if (isPrompt && this.wasInPrompt && ("" + o).equals(this.promptStr)) {
            return;
        }
        if (this.wasInPrompt) {
            System.out.println();
        }
        this.promptStr = isPrompt ? "" + o : "";
        super.p(o, isPrompt);
        if (!isPrompt && this.wasInPrompt) {
            super.p(this.promptStr, true);
        }
        this.wasInPrompt = isPrompt;
    }

    @Override
    protected void run() {
        Scanner scanner = new Scanner(System.in);
        String cmd;
        while (!this.stopCondition) {
            this.p("Enter command: ", true);
            try {
                while (System.in.available() == 0) {
                    Thread.sleep(1000);
                    if (this.stopCondition) {
                        return;
                    }
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                break;
            }
            try {
                cmd = scanner.nextLine();
                switch (cmd) {
                    case "quit":
                    case "exit":
                        this.stopCondition = true;
                        break;
                    default:
                        // Translate command into literal chaincode function to be called + params
                        String[] cmdParts = cmd.split(" ");
                        CcFunction function = CcFunction.fromString(cmdParts[0]);
                        if (function == null) {
                            this.p("Invalid command or function: " + cmdParts[0], false);
                            continue;
                        }
                        String[] args = Arrays.copyOfRange(cmdParts, 1, cmdParts.length);
                        this.p(this.invokeCC(function, args));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        new CLIClient(args.length == 0 ? "client_settings.json" : args[0]).run();
    }
}

package client;

import client.user.UserContext;
import client.util.Util;
import java.util.stream.IntStream;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.sdk.NetworkConfig.OrgInfo;
import org.hyperledger.fabric.sdk.exception.*;
import org.hyperledger.fabric.sdk.identity.X509Enrollment;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.hyperledger.fabric_ca.sdk.HFCAClient;
import org.hyperledger.fabric_ca.sdk.RegistrationRequest;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.security.PrivateKey;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Pattern;

import static java.nio.charset.StandardCharsets.UTF_8;
import static shared.GlobalConfig.*;


// The base class that all Clients inherit from. Contains lots of boilerplate code so that you don't have to write it yourself.
public abstract class PiClient {

    public static final boolean LOG_TO_FILE = true;
    public static final int INVOCATION_TIMEOUT_S = 600;
    public static final int N_INVOCATION_RETRIES = 3;

    private final PrintStream out = System.out;
    private final HashSet<String> receivedValidBlockEventTxIds = new HashSet<>();
    private final HashSet<String> receivedInvalidBlockEventTxIds = new HashSet<>();
    private final HashSet<String> receivedCCEventTxIds = new HashSet<>();
    private final HashMap<String, Long> txSendTimes = new HashMap<>();
    private final ConcurrentLinkedQueue<String> pendingTxs = new ConcurrentLinkedQueue<>();
    protected JSONObject jo;
    protected volatile boolean stopCondition = false;
    ConcurrentLinkedQueue<JSONObject> jsonChaincodeEvents = new ConcurrentLinkedQueue<>();

    private HFClient hfClient;
    private HFCAClient hfcaClient;
    private Channel channel;
    private String affiliation;
    private String ca;
    private String ccName;
    private String signedCertFile;
    private String privateKeyFile;
    private String msp;
    private OrgInfo orgInfo;
    private String currentNetworkId;
    private String username;
    private String logFileName;
    private String eventLogFileName;

    protected PiClient(String jsonConfigFile) {
        setup(jsonConfigFile);
        setTxConfirmationListener();
    }

    private static void logToFile(String fileName, String contents) {
        try {
            PrintStream fileStream = new PrintStream(new FileOutputStream(fileName, true));
            fileStream.append(contents).append('\n');
            fileStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected abstract void run();

    @SneakyThrows
    private void setup(String jsonConfigFile) {
        File jsonFile = new File(jsonConfigFile);
        jo = new JSONObject(FileUtils.readFileToString(jsonFile, "utf-8"));
        logFileName = jo.getString("logfile");
        eventLogFileName = jo.getString("eventlogfile");
        emptyFile(logFileName);
        emptyFile(eventLogFileName);

        NetworkConfig nc = NetworkConfig.fromJsonFile(new File(jo.getString("connection_profile")));
        affiliation = jo.getString("affiliation");
        ccName = jo.getJSONObject("chaincode").getString("name");
        privateKeyFile = jo.getString("keyfile");
        signedCertFile = jo.getString("certfile");
        username = jo.getString("username");

        orgInfo = nc.getOrganizationInfo(jo.getString("organization"));
        msp = orgInfo.getMspId();

        hfClient = HFClient.createNewInstance();
        hfClient.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());
        hfcaClient = HFCAClient.createNewInstance(orgInfo.getCertificateAuthorities().get(0));
        hfcaClient.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());

        currentNetworkId = jo.getString("network_version");
        Util.setWorkingDirectory(jo.getString("working_directory"));
        storeNewCredentialsIfNeeded();
        hfClient.setUserContext(Objects.requireNonNull(Util.readUserContext(affiliation, username)));
        String channelName = nc.getChannelNames().iterator().next();
        hfClient.loadChannelFromConfig(channelName, nc);
        channel = hfClient.getChannel(channelName);

        //===
        enrollUser("TESTTEST4");
    }

    @SneakyThrows
    private void storeNewCredentialsIfNeeded() {
        UserContext userContext = Util.readUserContext(affiliation, username);
        if (!currentCredentialsAreValid(userContext)) {
            Util.cleanUp();
            p("Storing user enrollment for " + username + " of " + affiliation);
            storeUser();
        } else {
            p("Found existing enrollment for " + username);
        }
    }

    private boolean currentCredentialsAreValid(UserContext userContext) {
        String networkId = Optional.ofNullable(userContext).map(UserContext::getNetworkId).orElse(null);
        return currentNetworkId.equals(networkId);
    }

    void eventLog(String s) {
        logToFile(eventLogFileName, s);
    }

    private void emptyFile(String logFileName) {
        try {
            new PrintWriter(logFileName).close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @SneakyThrows
    private String readFile(String filename) {
        FileInputStream stream = new FileInputStream(filename);
        String result = IOUtils.toString(stream, UTF_8);
        stream.close();
        return result;
    }

    @SneakyThrows
    private void storeUser() {
        UserContext userContext = new UserContext();
        userContext.setName(username);
        userContext.setAffiliation(affiliation);
        userContext.setNetworkId(currentNetworkId);
        userContext.setMspId(msp);

        String publicKeyStr = readFile(signedCertFile);
        String privateKeyStr = readFile(privateKeyFile);

        Reader pemReader = new StringReader(privateKeyStr);
        PEMParser pemParser = new PEMParser(pemReader);
        PrivateKeyInfo pemPair = (PrivateKeyInfo) pemParser.readObject();
        pemParser.close();
        PrivateKey privateKey = (new JcaPEMKeyConverter()).getPrivateKey(pemPair);
        userContext.setEnrollment(new X509Enrollment(privateKey, publicKeyStr));

        Util.writeUserContext(userContext);
    }

    @SneakyThrows
    private void enrollUser(String username) {
        UserContext adminContext = new UserContext();
        adminContext.setName("Admin");
        adminContext.setAffiliation(orgInfo.getName());
        adminContext.setNetworkId(currentNetworkId);
        adminContext.setMspId(msp);

        Enrollment adminEnrollment = hfcaClient.enroll("admin", "adminpw");
        adminContext.setEnrollment(adminEnrollment);
        Util.writeUserContext(adminContext);

        p("1");
        RegistrationRequest rr = new RegistrationRequest(username, affiliation);
        String enrollmentSecret = hfcaClient.register(rr, adminContext);

        UserContext userContext = new UserContext();
        userContext.setName(username);
        userContext.setAffiliation(orgInfo.getName());
        userContext.setNetworkId(currentNetworkId);
        userContext.setMspId(msp);
        Enrollment enrollment = hfcaClient.enroll(username, enrollmentSecret);
        userContext.setEnrollment(enrollment);

        hfClient.setUserContext(userContext);
    }

    protected void initChannel() throws BaseException {
        channel.initialize();
    }

    // Convenient wrapper for registering chaincodeEventListeners. You can probably ignore this function, unless you have a burning desire to create custom listeners.
    private void setEventListener(String eventName, ChaincodeEventListener chaincodeEventListener)
        throws InvalidArgumentException {
        channel.registerChaincodeEventListener(Pattern.compile(Pattern.quote(ccName)),
            Pattern.compile(Pattern.quote(eventName)), chaincodeEventListener);
    }

    // Register an event listener for BlockEvents, which will be used for detecting finished/failed transactions.
    @SneakyThrows
    private void setTxConfirmationListener() {
        channel.registerBlockListener(this::processBlockEvent);
    }

    // Looks at the transactions that are stored in a block, and checks if any pending transactions are now finished.
    private void processBlockEvent(BlockEvent blockEvent) {
        for (BlockEvent.TransactionEvent txEvent : blockEvent.getTransactionEvents()) {
            String txId = txEvent.getTransactionID();
            // Check if we have processed it before, and add it to the right set.
            boolean isNew = txEvent.isValid() ? addValidBlockEvent(txId) : addInvalidBlockEvent(txId);
            // If this event is about a pending transaction, we must mark it as done by removing it from the list (even if invalid)
            markAsDone(txEvent, txId);
            if (isNew) {
                printResponseTime(txId);
            }
        }
    }

    private synchronized boolean addInvalidBlockEvent(String txId) {
        boolean isNew = !receivedInvalidBlockEventTxIds.contains(txId);
        if (isNew) {
            p("Detected invalid transaction " + txId + "!");
            receivedInvalidBlockEventTxIds.add(txId);
        }
        return isNew;
    }

    private synchronized boolean addValidBlockEvent(String txId) {
        boolean isNew = !receivedValidBlockEventTxIds.contains(txId);
        if (isNew) {
            receivedValidBlockEventTxIds.add(txId);
        }
        return isNew;
    }

    private synchronized void markAsDone(BlockEvent.TransactionEvent txEvent, String txId) {
        if (pendingTxs.contains(txId)) {
            p("Pending transaction " + txId + " completed");
            pendingTxs.remove(txEvent.getTransactionID());
        }
    }

    private synchronized void printResponseTime(String txId) {
        if (txSendTimes.containsKey(txId)) {
            long currentTime = System.nanoTime();
            long txSendTime = txSendTimes.get(txId);
            p("Event response time for " + txId + " was " + (currentTime - txSendTime) + " nanoseconds!");
        }
    }

    // Adds an individual Chaincode event (in the form of a JSON object) to the queue.
    private void addJsonChaincodeEvent(String key, JSONObject jo) {
        jo.put("eventName", key);
        jsonChaincodeEvents.add(jo);
    }

    // Sets an event listener that listens for composite events and adds their components to this.jsonChaincodeEvents, while filtering out duplicates.
    protected void setDefaultCompositeEventListener(String eventName) throws InvalidArgumentException {
        setEventListener(COMPOSITE_EVENT,
            (handle, blockEvent, chaincodeEvent) -> {
                // The BlockEvent will be used to derive which transactions have been completed or failed
                processBlockEvent(blockEvent);

                String txId = chaincodeEvent.getTxId();
                if (ccEventIsValid(txId)) {
                    return;
                }
                // If we have received this event before, ignore it
                // This happens when more than 1 peer acts as an event source
                if (!ccEventIsNew(txId)) {
                    return;
                }
                receivedCCEventTxIds.add(txId);
                JSONArray eventArray = extractEventsJsonFromCcEvent(chaincodeEvent);
                IntStream.range(0, eventArray.length())
                    .mapToObj(eventArray::getJSONObject)
                    .forEach(eventJson -> processEventJson(eventJson, eventName));
            }
        );
    }

    private void processEventJson(JSONObject eventJson, String targetEventName) {
        if (targetEventName == null) {
            storeAllEvents(eventJson);
        } else if (eventJson.has(targetEventName)) {
            // Store all events that have the name eventName
            addJsonChaincodeEvent(targetEventName, eventJson.getJSONObject(targetEventName));
            p("Received an event we were listening for: " + eventJson.getJSONObject(targetEventName));
        }
    }

    private void storeAllEvents(JSONObject eventJson) {
        eventJson.keys().forEachRemaining(
            key -> {
                eventLog("{\"" + key + "\": " + eventJson.getJSONObject(key) + "}");
                addJsonChaincodeEvent(key, eventJson.getJSONObject(key));
            }
        );
    }

    private JSONArray extractEventsJsonFromCcEvent(ChaincodeEvent chaincodeEvent) {
        JSONObject compositeEvent = new JSONObject(new String(chaincodeEvent.getPayload()));
        eventLog(compositeEvent.toString());
        return compositeEvent.getJSONArray("events");
    }

    private synchronized boolean ccEventIsNew(String txId) {
        return !receivedCCEventTxIds.contains(txId);
    }

    private synchronized boolean ccEventIsValid(String txId) {
        return !receivedInvalidBlockEventTxIds.contains(txId);
    }

    // Wrappers for listening to all events.
    protected void setDefaultAnyCompositeEventListener() throws InvalidArgumentException {
        setDefaultCompositeEventListener(null);
    }

    // Convenience wrappers for invokeCC
    protected String invokeCC(CcFunction function, String... args) {
        return invokeCC(function, channel.getPeers(EnumSet.of(Peer.PeerRole.ENDORSING_PEER)), args);
    }

    protected String invokeCC(CcFunction function, Collection<Peer> peers, String... args) {
        return invokeCC(function, peers, N_INVOCATION_RETRIES, args);
    }

    protected String invokeCC(CcFunction function, int nRetries, String... args) {
        return invokeCC(function, channel.getPeers(EnumSet.of(Peer.PeerRole.ENDORSING_PEER)), nRetries, args);
    }

    protected String invokeCC(CcFunction function, boolean blocking, String... args) {
        return invokeCC(function, channel.getPeers(EnumSet.of(Peer.PeerRole.ENDORSING_PEER)), blocking, args);
    }

    private String invokeCC(CcFunction function, int nRetries, boolean blocking, String... args) {
        return invokeCC(function, channel.getPeers(EnumSet.of(Peer.PeerRole.ENDORSING_PEER)), nRetries, blocking,
                args);
    }

    private String invokeCC(CcFunction function, Collection<Peer> peers, boolean blocking, String... args) {
        return invokeCC(function, peers, N_INVOCATION_RETRIES, blocking, args);
    }

    protected String invokeCC(CcFunction function, Collection<Peer> peers, int nRetries, String... args) {
        return invokeCC(function, peers, nRetries, true, args);
    }

    // Perform a Chaincode invocation.
    // Args:
    //   - function: member of a custom enum in shared.GlobalConfig, make sure to register your own Chaincode functions there
    //   - peers: The set of peers to request endorsement from; you can probably leave this alone (ie. call this function without providing it),
    //         unless you want to limit the number of peers to enhance performance
    //         DEFAULT: all endorsing peers in the channel.
    //   - nRetries: The number of retries of this invocation, in case it fails.
    //         DEFAULT: N_INVOCATION_RETRIES
    //   - blocking: If set to false, the function will return after obtaining endorsements. That means the invocation will not have been confirmed!
    //         Additionally, no retries will be attempted if set to false (you could change the code to support this).
    //         DEFAULT: true
    //   - args: The parameters for the Chaincode invocation
    // Returns:
    //   - The reply to the invocation
    private String invokeCC(CcFunction function, Collection<Peer> peers, int nRetries, boolean blocking,
        String... args) {
        long startTime = System.nanoTime();
        String dump = dumpInvocation(function, args);

        try {
            TransactionProposalRequest request = makeInvocationRequest(function, args);
            Collection<ProposalResponse> response = channel.sendTransactionProposal(request, peers);
            long stopTime = System.nanoTime();
            String txId = response.iterator().next().getTransactionID();
            txSendTimes.put(txId, stopTime);
            long timePassed = stopTime - startTime;
            p("Sending PROPOSAL for " + dump + "(" + txId + ") took " + timePassed + " nanoseconds!");
            return processProposalResponse(response, blocking);
        } catch (ProposalException e) {
            p(dump + " failed, key collision?  " + nRetries + " attempts to go!");
            if (nRetries == 0) {
                p(e.getMessage());
                e.printStackTrace();
                return "";
            } else {
                return invokeCC(function, peers, nRetries - 1, blocking, args);
            }
        } catch (InvalidArgumentException e) {
            return dump + " failed because it was invalid!";
        }
    }

    private String dumpInvocation(CcFunction function, String... args) {
        return "invokeCC(" + function.getName() + ", " + String.join(", ", args) + ")";
    }

    private TransactionProposalRequest makeInvocationRequest(CcFunction function, String[] args)
        throws InvalidArgumentException {
        TransactionProposalRequest request = hfClient.newTransactionProposalRequest();
        ChaincodeID ccid = ChaincodeID.newBuilder().setName(ccName).build();
        request.setChaincodeID(ccid);
        request.setProposalWaitTime(1000000);
        request.setFcn(function.getName());
        request.setArgs(args);
        request.setChaincodeLanguage(TransactionRequest.Type.JAVA);
        Map<String, byte[]> tm = new HashMap<>();
        tm.put("HyperLedgerFabric", "TransactionProposalRequest:JavaSDK".getBytes(UTF_8));
        tm.put("method", "TransactionProposalRequest".getBytes(UTF_8));
        request.setTransientMap(tm);
        return request;
    }

    private String processProposalResponse(Collection<ProposalResponse> response, boolean blocking)
        throws ProposalException {
        String result = null, txId = null;
        // What do all of these peers think of this idea?
        for (ProposalResponse pres : response) {
            result = pres.getMessage();
            txId = pres.getTransactionID();
            try {
                pres.getChaincodeActionResponsePayload();
            } catch (InvalidArgumentException e) {
                p("Transaction proposal FAILED on channel " + channel.getName() + " by peer " + pres.getPeer()
                    .getName() + "!\n");
                p("With message: " + pres.getMessage() + "\n" +
                    "Status: " + pres.getStatus() + "\n" +
                    "And transaction id: " + pres.getTransactionID());
            }
        }
        // Send off to the ordering service!
        pendingTxs.add(txId);
        channel.sendTransaction(response);
        long startWaitingTime = System.currentTimeMillis();
        if (blocking) {
            // Wait until the transaction gets confirmed
            while (true) {
                synchronized (pendingTxs) {
                    if (!pendingTxs.contains(txId)) {
                        break;
                    }
                }
                long timeWaitedSeconds = (System.currentTimeMillis() - startWaitingTime) / 1000;
                if (timeWaitedSeconds > INVOCATION_TIMEOUT_S) {
                    throw new ProposalException("Timeout reached without transaction confirmation!");
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ignored) {
                }
            }
            if (receivedInvalidBlockEventTxIds.contains(txId)) {
                throw new ProposalException("Transaction returned as invalid!");
            }
        }
        return result;
    }

    public void p(Object o) {
        p(o, false);
    }

    // Wrapper for System.out.print(ln).
    // Is synchronized to prevent mangled output, so multiple threads may use this function on the same object.
    // Does not add a newline if isPrompt is true.
    protected void p(Object o, boolean isPrompt) {
        if (o instanceof String && ((String) o).length() == 0) {
            return;
        }
        String output = "[" + getClass().getName() + "]: " + o;
        synchronized (out) {
            if (!isPrompt) {
                output += '\n';
            }
            out.print(output);
            out.flush();
        }

        if (LOG_TO_FILE) {
            logToFile(logFileName, output);
        }
    }
}

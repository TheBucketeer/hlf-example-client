package client;

import java.util.Objects;
import org.json.JSONObject;

import static java.lang.Thread.sleep;

public abstract class PiClientEventListener implements Runnable {
    protected abstract PiClient getClient();

    protected abstract void handleEvent(JSONObject event) throws InterruptedException;

    protected void p(Object o) {
        System.out.println("[" + this.getClass().getName() + "]: " + o);
    }

    @Override
    public void run() {
        try {
            sleep(1000);
            while (!this.getClient().stopCondition) {
                if (this.getClient().jsonChaincodeEvents.size() == 0) {
                    sleep(100);
                } else {
                    JSONObject j = Objects.requireNonNull(this.getClient().jsonChaincodeEvents.poll());
                    this.getClient().eventLog("Event received: " + j.toString());
                    this.handleEvent(j);
                }
            }
            this.p("Stopped.");
        } catch (InterruptedException e) {
            this.p(e.getMessage());
            this.getClient().stopCondition = true;
        }
    }
}
